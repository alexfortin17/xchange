#pragma once
#include <string>

/*! @file */


/*! @enum OrderType
 * @brief Order Types enumeration
 */ 
enum OrderType
{
    MARKET, /*!< this is MARKET ORDER*/
    LIMIT, /*!< this is LIMIT ORDER*/
};

/*! @enum OrderSide
 * @brief Order Side enumeration
 */ 
enum OrderSide
{
    BUY, /*!< this is MARKET ORDER*/
    SELL, /*!< this is LIMIT ORDER*/
};

/*! Order class */
class Order
{   
    protected:
        OrderSide order_side;
        OrderType order_type;
        std::string ticker;
        unsigned int amount;
        double price;
        bool filled;


    public:

        /**
         * @brief Constructor for Order
         * @param oside side of the order (Buy or sell)
         * @param otype type of the order (market, limit, etc)
         */
        Order(OrderSide oside, OrderType otype, std::string ticker, unsigned int amount, double price = 0.0, bool filled = false);

        bool is_filled(void) const;

        OrderSide get_order_side(void) const;

        OrderType get_order_type(void) const;

        std::string get_ticker(void) const;

        unsigned int get_amount(void) const;

        double get_price(void) const;

        /**
         * @brief fill this order
         * @return void
         */
        //virtual void fill(void) = 0;
};