#pragma once
#include "order.hh"

class MarketOrder : public Order
{
    public:

        MarketOrder(OrderSide oside, std::string ticker, unsigned int amount, double price = 0.0, bool filled = false);
};