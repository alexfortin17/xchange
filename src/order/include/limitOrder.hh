#pragma once
#include "order.hh"

class LimitOrder : public Order
{
    public:

        LimitOrder(OrderSide oside, std::string ticker, unsigned int amount, double price, bool filled = false);
};