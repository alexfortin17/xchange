#pragma once

#include <memory>

#include "limitOrder.hh"

std::shared_ptr<const LimitOrder> fill(const LimitOrder* order);