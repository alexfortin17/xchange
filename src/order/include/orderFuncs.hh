#include "order.hh"
#include <memory>


bool is_filled(std::shared_ptr<const Order> order);

template <typename T>
std::shared_ptr<const T> fill(const T* order);

#include "orderFuncs.tcc"