#pragma once
#include <memory>

template <typename T>
std::shared_ptr<const T> fill(const T* order){
    T modif_order(order->get_order_side(), order->get_ticker(), order->get_amount(), order->get_price(), true);
    return std::make_shared<T>(modif_order);
}