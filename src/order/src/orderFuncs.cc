#include "orderFuncs.hh"
#include <iostream>

bool is_filled(std::shared_ptr<const Order> order){
    return order->is_filled();
}

