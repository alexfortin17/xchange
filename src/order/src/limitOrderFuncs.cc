#include "limitOrderFuncs.hh"
#include <iostream>

std::shared_ptr<const LimitOrder> fill(const LimitOrder* order){
    std::cout << "Filling Limit Order" << std::endl;
    LimitOrder modif_order(order->get_order_side(), order->get_ticker(), order->get_amount(), order->get_price(), true);
    return std::make_shared<LimitOrder>(modif_order);
}