#include "order.hh"


Order::Order(OrderSide oside, OrderType otype, std::string ticker, unsigned int amount, double price, bool filled): 
order_side(oside), order_type(otype),ticker(ticker), amount(amount), price(price), filled(filled) {}

bool Order::is_filled(void) const{
    return this->filled;
}

OrderSide Order::get_order_side(void) const{
    return this->order_side;
}

OrderType Order::get_order_type(void) const{
    return this->order_type;
}

std::string Order::get_ticker(void) const{
    return this->ticker;
}

unsigned int Order::get_amount(void) const{
    return this->amount;
}

double Order::get_price(void) const{
    return this->price;
}