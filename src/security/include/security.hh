#include <string>
#include <vector>
#include <memory>

/*! @enum SecurityType
 * @brief Security Type enumeration
 */ 
enum SecurityType
{
    STOCK, /*!< this is STOCK Security*/
    BOND, /*!< this is BOND Security*/
};

class Security{
    protected:
        unsigned int id;
        std::string ticker;
        unsigned int owner_id;
        std::vector<unsigned char> watermark;

    public:
        Security(unsigned int id, std::string ticker, unsigned int owner_id, std::vector<unsigned char> watermark);

        virtual SecurityType get_security_type(void) = 0;
};