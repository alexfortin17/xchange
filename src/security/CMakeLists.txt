add_library (Security src/security.cc)

target_include_directories(Security PRIVATE ${SECURITY_HEADERS})

target_compile_options (Security PRIVATE ${PROJECT_CXX_FLAGS})
target_include_directories(Security PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
