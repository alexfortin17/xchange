#include <openssl/rsa.h>
#include <string>

class Participant{
    protected:
    
        unsigned int id;
        std::string name;
        std::string pub_key;

    public:
        Participant(unsigned int id, std::string name, std::string pub_key);

        //Make it non copyable ==================
       // Participant(const Participant&) = delete;
       // void operator=(const Participant&) = delete;
        //========================================

};