#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <stdexcept>
#include <iostream>
#include "participantFuncs.hh"


std::string public_decrypt(std::string pub_key, std::vector<unsigned char> watermark){
    RSA* rsa_pub_key;
    BIO* pbio = BIO_new(BIO_s_mem());
    BIO_write(pbio, pub_key.c_str(), pub_key.length());
    PEM_read_bio_RSA_PUBKEY(pbio, &rsa_pub_key, NULL, NULL);
    if(rsa_pub_key == nullptr){
        throw std::runtime_error("An error occured reading the public key into a RSA struct");
    }

    unsigned char *wm_arr = &watermark[0];
    unsigned char result[512] = {};
    RSA_public_decrypt(watermark.size(), wm_arr, result, rsa_pub_key, RSA_PKCS1_PADDING);
    std::string dec_plaintext((char*)result);
    BIO_free(pbio);
    RSA_free(rsa_pub_key);
    return dec_plaintext;
}