#include <iostream>
#include <vector>
#include <algorithm>

#include "order.hh"
#include "orderFuncs.hh"
#include "marketOrder.hh"
#include "marketOrderFuncs.hh"
#include "limitOrder.hh"
#include "participant.hh"
#include "participantFuncs.hh"


/**
 * RAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
 */
int main(){
    std::string ibm = "IBM";
    //buy 1 share of IBM
    const MarketOrder orderx(BUY, ibm, 1);
    std::cout << is_filled(std::make_shared<const Order>(orderx)) << std::endl;
    std::shared_ptr<const Order> ordery = fill(&orderx);
    std::cout << is_filled(ordery) << std::endl;

    std::string jpm = "JPM";
    //sell 1 share of JPM with limit of 100$
    const LimitOrder ordera(SELL, jpm, 1, 100);
    std::cout << is_filled(std::make_shared<const Order>(ordera)) << std::endl;
    std::shared_ptr<const Order> orderb = fill(&ordera);
    std::cout << is_filled(orderb) << std::endl;




    const std::string pkey = "-----BEGIN PUBLIC KEY-----\n"\
    "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAxBZzLoGbSM/zdM7Dmmsa\n"\
    "GQMfADv4AKnzRF38qoNP4/7J5g9aOv9WbTd+8O5rvZhprC49EGJ5iFluLcSjiqGW\n"\
    "x/4QBi6gHbkFb8zA+NeX0oK/oekJmLD2OB0kvYiFeJ1iXg4nIIpa5mZXDs452RC3\n"\
    "Scoa10tl4AfHxEB3Rv3d2s8EjtsSuzhl9lycWN0l2G+b5E2WS7YAVZdt5EIpPH2+\n"\
    "Fn/Sc/EUf7o33JevIozo3EAKZyVtWLiMTGky4CcN/bI5/h6G5Yufmxk4slnXlP0X\n"\
    "j/VQpkSRdmF9J9I9bDyiOwGozWvhQ7xc8K62TKYy00eaKaKr8pmq6cUt/l8Q4Q8W\n"\
    "Ug7IvRjM1ujyETm/nAyfVsPqXMhneVG/l0KordN5lC8pjA+04ywNIezByJV0LF2X\n"\
    "SyI0YoBCxJ7/ExspOadCo+jpc/Zh+72qr8rOttaOORlMry7SPQheLSMASXmzobUD\n"\
    "X1QvNPLpbhJbFarEzWALKSz0OuGwtqIzhFOEDgaVLTvUN14l5VMrgJbGMyDOB2Vr\n"\
    "9Rarv045XBxRq1h6rn/+bcCSpJqgIc1uq5yttYoG6wQuvset3RfsBgTsTiSUEaZ1\n"\
    "WgZs3Gv64RhqwMVEIaJDe9bbb9en3PoBnwz1IvV43UJgMTjCU3s5A9rEKzt7h8gJ\n"\
    "haX0X26q9JMu/sNfeRCvmecCAwEAAQ==\n"\
    "-----END PUBLIC KEY-----\n";

    Participant alexfortin(1, "Alex Fortin", pkey);

    return 0;
}
