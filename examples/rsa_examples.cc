#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/evp.h>
#include <string>
#include <cstring>
#include <iostream>



int main(){
  //FILE *keyfile = fopen(".ssh/id_rsa.pub", "r");
  //RSA *rsaPubKey = PEM_read_bio_RSA_PUBKEY(keyfile, NULL, NULL, NULL);

  const std::string pkey = "-----BEGIN PUBLIC KEY-----\n"\
    "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAxBZzLoGbSM/zdM7Dmmsa\n"\
    "GQMfADv4AKnzRF38qoNP4/7J5g9aOv9WbTd+8O5rvZhprC49EGJ5iFluLcSjiqGW\n"\
    "x/4QBi6gHbkFb8zA+NeX0oK/oekJmLD2OB0kvYiFeJ1iXg4nIIpa5mZXDs452RC3\n"\
    "Scoa10tl4AfHxEB3Rv3d2s8EjtsSuzhl9lycWN0l2G+b5E2WS7YAVZdt5EIpPH2+\n"\
    "Fn/Sc/EUf7o33JevIozo3EAKZyVtWLiMTGky4CcN/bI5/h6G5Yufmxk4slnXlP0X\n"\
    "j/VQpkSRdmF9J9I9bDyiOwGozWvhQ7xc8K62TKYy00eaKaKr8pmq6cUt/l8Q4Q8W\n"\
    "Ug7IvRjM1ujyETm/nAyfVsPqXMhneVG/l0KordN5lC8pjA+04ywNIezByJV0LF2X\n"\
    "SyI0YoBCxJ7/ExspOadCo+jpc/Zh+72qr8rOttaOORlMry7SPQheLSMASXmzobUD\n"\
    "X1QvNPLpbhJbFarEzWALKSz0OuGwtqIzhFOEDgaVLTvUN14l5VMrgJbGMyDOB2Vr\n"\
    "9Rarv045XBxRq1h6rn/+bcCSpJqgIc1uq5yttYoG6wQuvset3RfsBgTsTiSUEaZ1\n"\
    "WgZs3Gv64RhqwMVEIaJDe9bbb9en3PoBnwz1IvV43UJgMTjCU3s5A9rEKzt7h8gJ\n"\
    "haX0X26q9JMu/sNfeRCvmecCAwEAAQ==\n"\
    "-----END PUBLIC KEY-----\n";

  RSA* rsa_pub_key;
  BIO* public_bio = BIO_new(BIO_s_mem());
  BIO_write(public_bio, pkey.c_str(), pkey.length());
  PEM_read_bio_RSA_PUBKEY(public_bio, &rsa_pub_key, NULL, NULL);

  if(rsa_pub_key == nullptr){
    std::cout << "An error occured loading the public key. Exiting..." << std::endl;
    return 1;
  }
  
  const std::string prikey = "-----BEGIN PRIVATE KEY-----\n"\
    "MIIJQQIBADANBgkqhkiG9w0BAQEFAASCCSswggknAgEAAoICAQDEFnMugZtIz/N0\n"\
    "zsOaaxoZAx8AO/gAqfNEXfyqg0/j/snmD1o6/1ZtN37w7mu9mGmsLj0QYnmIWW4t\n"\
    "xKOKoZbH/hAGLqAduQVvzMD415fSgr+h6QmYsPY4HSS9iIV4nWJeDicgilrmZlcO\n"\
    "zjnZELdJyhrXS2XgB8fEQHdG/d3azwSO2xK7OGX2XJxY3SXYb5vkTZZLtgBVl23k\n"\
    "Qik8fb4Wf9Jz8RR/ujfcl68ijOjcQApnJW1YuIxMaTLgJw39sjn+Hobli5+bGTiy\n"\
    "WdeU/ReP9VCmRJF2YX0n0j1sPKI7AajNa+FDvFzwrrZMpjLTR5opoqvymarpxS3+\n"\
    "XxDhDxZSDsi9GMzW6PIROb+cDJ9Ww+pcyGd5Ub+XQqit03mULymMD7TjLA0h7MHI\n"\
    "lXQsXZdLIjRigELEnv8TGyk5p0Kj6Olz9mH7vaqvys621o45GUyvLtI9CF4tIwBJ\n"\
    "ebOhtQNfVC808uluElsVqsTNYAspLPQ64bC2ojOEU4QOBpUtO9Q3XiXlUyuAlsYz\n"\
    "IM4HZWv1Fqu/TjlcHFGrWHquf/5twJKkmqAhzW6rnK21igbrBC6+x63dF+wGBOxO\n"\
    "JJQRpnVaBmzca/rhGGrAxUQhokN71ttv16fc+gGfDPUi9XjdQmAxOMJTezkD2sQr\n"\
    "O3uHyAmFpfRfbqr0ky7+w195EK+Z5wIDAQABAoICABaPTqknGCqEYkoqD68Qa2w9\n"\
    "Q6/PNnU9GFPhTFSBzws4IRpq4cKv5c2i+OPXSmYBxwacO0JLUhgx0c9KNFaayLIx\n"\
    "tL5BVwKUat+1+u2mQD1j9ZA3M3dxxKIoGDYGIvUBhnOzxSSlRlgDpjh52YXzumKu\n"\
    "8e+bKRlhS8Z0dESiqIeoEaot4jXQA0sY1JQUvWntTU4y7vu+8MLoLSh5xCzFer0z\n"\
    "746PSgB3+D1U5j1fYNsWMxwy3iW69clEwSmw1Jc/jhSChpqaOb6qZC+AzqDBjZhu\n"\
    "bNz0PpgUF0YhlheB0lhflIAtzTjxzu+R6j9fT7GnJ0FrCcVbkKJcoTIG1fZOug57\n"\
    "MrEpqYD35MAU35T0R5LnyZ/O/sxAzVpmEIk6/gSgO3JzcdFt6XwrYex3rx8ynG3c\n"\
    "drcIcnqXzHGdBtZZiSI5fhtqmrpkBx0Fg9kDt+S6ou8wqoD6DS7wreKdN9BQorDJ\n"\
    "VrkuWTXR/vFC/LaaQbeVEnv/jJIrx6L8EXLKIIcqzmbGxxgvroKVe4D2iW7Yq9ul\n"\
    "sb93Se5GjqujFlVkPLeJG79ZvkPMhaormN0lSYVZ688PEylg5ERr/iJmwqjo0w9g\n"\
    "0GzWxbkekTD7g4cVullXfKvLH66/4XUu/EPwyzzt1i9eBVin6VmexH8Z1k8O0NIe\n"\
    "wE/IwP4XtaACHr0qkvmBAoIBAQDp8J+gPj9qaBlZcK3/+UTAA5iOvWBxlAVjBmbV\n"\
    "JoXXkrZP4/iXUVTW9ada+qe1MLcNsaHFpDUX97DfMU618wNQP61vwbxiRLabDlTY\n"\
    "Lv9+TnEzOTTz2t7jlQVPVJukYsH+jpQGZila1XakLEbFJvzWN0avVGImgyYlW5ke\n"\
    "JLAc5WEHUeYT4wEu7MkUILtEBYv0bsuRr2Sfisyz6HVuzAOFKeyhvEmZgynRgFvY\n"\
    "oQdN4B8vhMtFTrEElCQli47z76EnQhD09g0U7wNZ6xjq5mTyFP1MKvwSN4AuBNai\n"\
    "3F9PSSZEZi2vFUoq3Np+pCR9RG0qxIaW6mT8rerFLbllT943AoIBAQDWlBADiO8t\n"\
    "8iJ3EViqCI9AudPSfzUAcC41ox25OSEkqIjrRA1+OeWjGn8nVuzDtgK78ZcQZjuF\n"\
    "KQQ0WQrYVvganwQF8SfngI5rV5aRA89z5D2WxR0clo6O1tnbhP+H5seR8Xw4E8N0\n"\
    "IoF9XJR+FsR8bD865bXYqCAgJ6472WzYqpt+a7sc1Pur0ZcRntrLV1M3PbpBCoZX\n"\
    "iHV3jS6FyQjuSzuFQv6KybwmEPDpQsLdhDwVkpPtCp0ba14d02ugmLqBdzL6h+M2\n"\
    "4GPC7WKURrZoK4OHKOKMylsnrt/pQFrf1dBHLeG24p7twrglGEYw1aHvOJaq6Knp\n"\
    "OLOTwzlyDsnRAoIBAA9slwm4sPOyE2HLRR2ocLj8Q7W9lLtIWCaUSHlrRc3V1YFk\n"\
    "9YH8kcxue+9KsX4HzTnw6UqfrgR7h0Tf+mGSuTg0Spoguuv7vdJxWnS6fIv2Xg5T\n"\
    "ikM9vg5hR+1YFb775/CBtA7L85QSmPTwGmZrUKZLFLUAqkwBv6Uc6faL0rTFBuW1\n"\
    "p6bOA8j1Qbi7A73TY8vk9jUPzGGchCykegGpH95KylhLksq1VC+E+AlLbLDcwxGd\n"\
    "/sz+CDvWDKARGIbK2OQ+veF4y5oCvETUdFsgf61aZuz7tIz67c+a8A/i2cHff89o\n"\
    "sUBm9fHnh9xrtCAyP8RfGsSehbQHP4BqqFuerXcCggEAQ64ZyvOi9Sv9QO8At0jR\n"\
    "3kl+jVNAJW48tQBI/MYPNR/PZ91kIB8k1igiWQcq0KQVArC1xB9qwJpqUBSRviDY\n"\
    "SpVDUJh6zKMYyLSssfqwsU4ZOvfbMcwvFNChkILHklbxNZAkbQ+Ww4eqrh7IW2jU\n"\
    "+qWt3+baT4bf8yRw6UGv+jr9podWUka/Rrdp3c3sFtzpS3A10hP+wZcx7c5jESGS\n"\
    "po6mLGcwIBh3Ns57HifHjNULmoZMECUb3q0j12Z6+QGTOgid1LWNrh5iePYk9L3F\n"\
    "BOBMsRHtOxkMXVzkFQ8OWrF26yyGmAestzOGn3Sjlv2bAPgpVajjBsOyqHWYJeiS\n"\
    "MQKCAQBzy2Npn2ALhijmHDJxSMx55CHHeJi2vwfS5uaUXgEpEnq72EuB/s6qBiKu\n"\
    "fSwrhcjy6QbK49+ZoPEa3NbTNjtHju/ftHyYQsDrsWv1kA82jiAXyeO/wKt3swRC\n"\
    "YdICleKPJ5C/ytqv4rU+7E0sDUp+iSJG8tKVOXQUinneGRKPOQqQllwA38nIqw/g\n"\
    "OSwxJ/GPf9AFkgSM5/qT5uX6oTcY9H2vnfcF4Y/RVEfVzLKeq0GQstp3R4Bi5q9B\n"\
    "JyI9E0IyzTl9k5fbJLPUDeQmEwW9rSsHqxhBRH8e+aHAwxFUhyjOik4eEBp1sXmY\n"\
    "jvFG9F9ry9gJy3/7codDpldk7GgU\n"\
    "-----END PRIVATE KEY-----\n";

  RSA* rsa_pri_key = NULL;
  BIO* private_bio = BIO_new_mem_buf(prikey.c_str(), -1);
  PEM_read_bio_RSAPrivateKey(private_bio, &rsa_pri_key, NULL, NULL);

  if(rsa_pri_key == nullptr){
    std::cout << "An error occured loading the private key. Exiting..." << std::endl;
    return 1;
  }


  unsigned char orig[512] = "EnCrYptME?!";
  std::string orig_str((char*)orig);
  std::cout << "Plaintext: " << orig_str << std::endl;

  unsigned char buff[512] = {};
  int padding = RSA_PKCS1_PADDING;

  int encrypted_size = RSA_private_encrypt(strlen((char*)orig), orig, buff, rsa_pri_key, padding);
  std::string ciphertext((char*)buff);
  std::cout << "Ciphertext: " << ciphertext << std::endl;
  std::cout << "CText size: " << ciphertext.length() << std::endl;


  unsigned char buff2[512] = {};
  RSA_public_decrypt(encrypted_size, buff, buff2, rsa_pub_key, padding);
  std::string dec_plaintext((char*)buff2);
  std::cout << "Decrypted Plaintext: " << dec_plaintext << std::endl;

  BIO_free(public_bio);
  RSA_free(rsa_pub_key);
  BIO_free(private_bio);
  RSA_free(rsa_pri_key);
  return 0;

}
