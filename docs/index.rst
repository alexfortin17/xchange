.. Xchange documentation master file, created by
   sphinx-quickstart on Mon Jun  8 00:36:04 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Xchange's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Docs
====

.. doxygenclass:: Order::Order
  :members:
