.PHONY: default

default:
	mkdir -p build/
	cd build; cmake ../src
	make -j8 -C build/

.PHONY: clean

clean:
	(test ./build/Makefile && make -C build clean) || echo "No Makefile found in build"
